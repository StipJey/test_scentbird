import {
    OPEN_POPUP,
    OPEN_ALL_POPUP,
    CLOSE_POPUP,
} from '../actions'

export const openPopup = (id) => {
    return {
        type: OPEN_POPUP,
        id,
    }
}

export const closePopup = () => {
    return {
        type: CLOSE_POPUP,
    }
}

export const openAllPopups = () => {
    return {
        type: OPEN_ALL_POPUP,
    }
}
