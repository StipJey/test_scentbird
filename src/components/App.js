import React from 'react'
import Popup from './Popup'
import Page from '../containers/PageContainer'
import CSSModules from 'react-css-modules'
import styles from './App.css'

const App = ({opened_popups, popups_data, closePopup}) => {
    const firstPopupId = opened_popups[0];
    const firstPopupData = popups_data[firstPopupId];
    return (
    <div styleName="app">
        <Page />
        {firstPopupId && firstPopupData &&
            <Popup onClosePopup={closePopup} {...firstPopupData}/>
        }
    </div>
    )
}

App.propTypes = {
    opened_popups: React.PropTypes.array.isRequired,
    popups_data: React.PropTypes.object.isRequired,
    closePopup: React.PropTypes.func.isRequired,
}

export default CSSModules(App, styles)