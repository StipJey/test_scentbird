import React, { Component, PropTypes } from 'react'
import CSSModules from 'react-css-modules'
import styles from './Page.css'

class Page extends Component {

    constructor(...args) {
        super(...args);
        this.state = {
            openAll: false,
            selectedPopup: this.props.popups[0],
        }
    }

    buttonClick() {
        if (this.state.openAll) {
            this.props.boundOpenAllPopups();
        } else {
            this.props.boundOpenPopup(this.state.selectedPopup);
        }
    }

    select(event){
        this.setState({selectedPopup: event.target.value});
    }

    change(event){
        this.setState({openAll: !this.state.openAll});
    }

    render() {
        return (
            <div styleName="page">
                <div styleName="page__tabs">
                    <div styleName="tab">
                        Users
                    </div>
                    <div styleName="tab--active">
                        Statistics
                    </div>
                </div>
                <div styleName="page__body">
                    <div styleName="page__header">
                        <div styleName="title">
                            Some statistics
                        </div>
                    </div>
                    <div styleName="checkbox">
                        <input
                            type="checkbox"
                            id="checkbox"
                            checked={this.state.openAll}
                            onChange={(event) => {this.change(event)}}
                        />
                        <label styleName="label" htmlFor="checkbox">Open all popups</label>
                    </div>
                    <div styleName="select-box">
                        <select
                            styleName="select"
                            defaultValue={this.state.selectedPopup}
                            onChange={(event) => {this.select(event)}}
                        >
                            {this.props.popups.map((popupId, index) => {
                                return <option key={index} value={popupId}>{popupId}</option>;
                            })}
                        </select>
                        <button styleName="button" onClick={() => {this.buttonClick()}}>OPEN</button>
                    </div>
                </div>
            </div>
        )
    }
}

Page.propTypes = {
    popups: PropTypes.array.isRequired,
    boundOpenPopup: PropTypes.func.isRequired,
    boundOpenAllPopups: PropTypes.func.isRequired,
}

export default CSSModules(Page, styles)
