import React, { PropTypes } from 'react'
import CSSModules from 'react-css-modules'
import styles from './Popup.css'

const Popup = ({ title, text, onClosePopup }) => {
    return (
        <div styleName="popup-container">
            <div styleName="popup-overlay" onClick={onClosePopup} ></div>
            <div styleName="popup">
                <div styleName="popup__header">
                    <div styleName="popup__title">{title}</div>
                    <div styleName="popup__close-btn" onClick={onClosePopup}>x</div>
                </div>
                <div styleName="popup__body">
                    {text}
                </div>
                <button styleName="finish-btn" onClick={onClosePopup}>FINISH</button>
            </div>
        </div>
    )
}

Popup.propTypes = {
    onClosePopup: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
}

export default CSSModules(Popup, styles)
