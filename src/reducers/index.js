import {
    OPEN_POPUP,
    OPEN_ALL_POPUP,
    CLOSE_POPUP ,
} from '../actions';

const initialState = {
    opened_popups: [],
    popups_data: {
        popup1: {
            title: 'Popup 1',
            text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rutrum luctus lorem, ut porttitor leo accumsan in. Sed ipsum erat, consequat ut odio sed, mollis dictum nisi. Nulla blandit pulvinar libero, sit amet dictum nibh semper cursus. Nunc ligula justo, aliquam vel pellentesque feugiat, gravida ut risus. Donec rutrum, arcu sed congue feugiat, ipsum nibh vestibulum mi, nec tincidunt urna massa sit amet velit. Etiam quis commodo eros. Donec iaculis arcu eu consequat pellentesque. Vivamus lectus libero, malesuada in orci quis, dictum maximus arcu. Nulla sagittis venenatis massa ut placerat. Nulla sapien ipsum, convallis vitae facilisis sed, rhoncus eu tortor.',
        },
        popup2: {
            title: 'Popup 2',
            text: 'Nulla eu lacus ac diam condimentum venenatis. Nullam quis ultricies risus. Integer fringilla vitae neque quis molestie. Etiam est felis, malesuada at turpis sed, porttitor tristique turpis. Aliquam bibendum nulla pellentesque interdum mattis. Cras scelerisque ullamcorper scelerisque. Donec ultrices blandit dui, quis consequat lectus. Cras nec nisi turpis. Fusce eu pretium massa. Nunc dolor orci, gravida varius dictum vel, vehicula nec felis. Fusce commodo eu urna vitae mattis.',
        }
    }
};

export default function reducer(state = initialState, action) {
    switch(action.type) {
        case OPEN_POPUP: {
            return {
                ...state,
                opened_popups: state.opened_popups.concat([action.id]),
            }
        } break;
        case OPEN_ALL_POPUP: {
            return {
                ...state,
                opened_popups: Object.keys(state.popups_data),
            }
        } break;
        case CLOSE_POPUP: {
            return {
                ...state,
                opened_popups: state.opened_popups.slice(1),
            }
        } break;
        default:
            return state;
    }
}