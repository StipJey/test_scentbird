import { connect } from 'react-redux'
import { openPopup, openAllPopups } from '../actionCreators'
import Page from '../components/Page'

const mapStateToProps = (state) => {
    return {
        popups: Object.keys(state.popups_data)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        boundOpenPopup: (id) => {
            dispatch(openPopup(id))
        },
        boundOpenAllPopups: () => {
            dispatch(openAllPopups())
        }
    }
}

const PageContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Page)

export default PageContainer