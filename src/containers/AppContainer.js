import { connect } from 'react-redux'
import App from '../components/App'
import { closePopup } from '../actionCreators'

const mapStateToProps = (state) => {
    return {
        opened_popups: state.opened_popups,
        popups_data: state.popups_data,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        closePopup() {
            dispatch(closePopup())
        }
    }
}

const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(App)

export default AppContainer